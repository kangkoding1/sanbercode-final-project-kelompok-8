<?php
return [
	'image' => [
		'directory' => 'img',
		'thumbnail' => [
			'width' => 250,
			'height' => 170
		]
	],
	'gambarfrontend' => [
		'directory' => 'img',
	],
	'default_category_idnya' => 1,
	'default_user_idnya' => 1,
	'default_user_author_idnya' => 2,
];