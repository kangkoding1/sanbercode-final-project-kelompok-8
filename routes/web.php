<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'Backend\HomeController@index')->name('home');

Route::get('/', [
	'uses' => 'BlogController@index',
	'as' => 'blog',
]);

Route::get('/berita/{postings}', [
	'uses' => 'BlogController@tampil',
	'as' => 'blog.tampil',
]);

Route::get('/category/{category}', [
	'uses' => 'BlogController@kategori',
	'as' => 'category',
]);

Route::get('/berita', [
	'uses' => 'BlogController@berita',
	'as' => 'blog.berita',
]);

Route::get('/author/{author}', [
	'uses' => 'BlogController@author',
	'as' => 'author',
]);

Route::put('/backend/blog/restore/{blog}', [
	'as' => 'backend.blog.restore',
	'uses' => 'Backend\BlogController@restore',
]);

Route::delete('/backend/blog/force-destroy/{blog}', [
	'as' => 'backend.blog.force-destroy',
	'uses' => 'Backend\BlogController@forceDestroy',
]);

Route::put('/backend/prestasi/restore/{prestasi}', [
	'as' => 'backend.prestasi.restore',
	'uses' => 'Backend\PrestasiController@restore',
]);

Route::delete('/backend/prestasi/force-destroy/{prestasi}', [
	'as' => 'backend.prestasi.force-destroy',
	'uses' => 'Backend\PrestasiController@forceDestroy',
]);

Route::resource('/backend/blog', 'Backend\BlogController');

Route::resource('/backend/prestasi', 'Backend\PrestasiController');

Route::resource('/backend/categories', 'Backend\CategoriesController');

Route::resource('/backend/users', 'Backend\UsersController');

Route::resource('/backend/jurusan', 'Backend\JurusanController');

Route::resource('/backend/profil', 'Backend\ProfilController');

