<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use GrahamCampbell\Markdown\Facades\Markdown;

class Prestasi extends Model
{
    use SoftDeletes;

    public $table = "prestasi";
    
	protected $dates = ['published_at'];
    protected $fillable = ['title','slug','oleh','body','published_at', 'image','tingkat'];

	public function Author()
	{
		return $this->belongsTo(User::class);
	}

    public function getImageUrlAttribute($value)
    {
    	$gambarUrl = "";

    	if(!is_null($this->image)){
            $directory = config('cms.image.directory');
    		$gambarPath = public_path(). "/{$directory}/" . $this->image;
    		if(file_exists($gambarPath)){
    			$gambarUrl = asset("{$directory}/" . $this->image);
    		}
    	}

    	return $gambarUrl;
    }

    public function getImageThumbUrlAttribute($value)
    {
        
        $gambarUrl = "";

        if(!is_null($this->image)){
            $directory = config('cms.image.directory');
            $ekstensi = substr(strchr($this->image, '.'), 1);
            $thumbnail = str_replace(".{$ekstensi}", "_thumb.{$ekstensi}", $this->image);
            $gambarPath = public_path(). "/{$directory}/" . $thumbnail;
            if(file_exists($gambarPath)){
                $gambarUrl = asset("{$directory}/" . $thumbnail);
            }
        }

        return $gambarUrl;
    }

    public function getDateAttribute($value)
    {
    	return is_null($this->published_at) ? '' : $this->published_at->diffForHumans();
    }

    public function scopeLatestFirst()
    {
    	return $this->orderBy('published_at', 'DESC');
    }

    public function scopePopular()
    {
        return $this->orderBy('view_count', 'DESC');
    }

    public function scopePublished()
    {
    	return $this->where("published_at", "<=", Carbon::now());
    }

    public function getBodyHtmlAttribute($value)
    {
    	return $this->body ? Markdown::convertToHtml(e($this->body)) : NULL;
    }

    public function getExcerptHtmlAttribute($value)
    {
    	return $this->excerpt ? Markdown::convertToHtml(e($this->excerpt)) : NULL;
    }

    public function dateFormatted($showTimes = false)
    {
        $format = "d/m/Y";
        if($showTimes) $format = $format . " H:i:s";
        return $this->created_at->format($format);
    }

    public function publicationLabel()
    {
        if(! $this->published_at){
            return '<span class="badge badge-warning">Draft</span>';
        }elseif($this->published_at && $this->published_at->isFuture()){
            return '<span class="badge badge-info">Terjadwal</span>';
        }else{
            return '<span class="badge badge-success">Dipublikasikan</span>';
        }
    }

    public function setPublishedAtAttribute($value)
    {
        $this->attributes['published_at'] = $value ?: NULL;
    }

    public function scopeScheduled($query)
    {
        return $query->where("published_at", ">", Carbon::now());   
    }

    public function scopeDraft($query)
    {
        return $query->whereNull("published_at");
    }

}
