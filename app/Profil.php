<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use GrahamCampbell\Markdown\Facades\Markdown;

class Profil extends Model
{
    public $table = 'profil';
    protected $fillable = ['namasekolah','alamat','telp','sejarah','namaheader','gambarbg','gambarbg2','gambarbg3','logo','motto','visi','misi','fax','email'];

    public function getLogoUrlAttribute($value)
    {
    	$gambarUrl = "";

    	if(!is_null($this->logo)){
            $directory = config('cms.image.directory');
    		$gambarPath = public_path(). "/{$directory}/" . $this->logo;
    		if(file_exists($gambarPath)){
    			$gambarUrl = asset("{$directory}/" . $this->logo);
    		}
    	}

    	return $gambarUrl;
    }

    public function getGambarBgAUrlAttribute($value)
    {
        $gambarUrl = "";

        if(!is_null($this->gambarbg)){
            $directory = config('cms.image.directory');
            $gambarPath = public_path(). "/{$directory}/" . $this->gambarbg;
            if(file_exists($gambarPath)){
                $gambarUrl = asset("{$directory}/" . $this->gambarbg);
            }
        }

        return $gambarUrl;
    }

    public function getGambarBgBUrlAttribute($value)
    {
        $gambarUrl = "";

        if(!is_null($this->gambarbg2)){
            $directory = config('cms.image.directory');
            $gambarPath = public_path(). "/{$directory}/" . $this->gambarbg2;
            if(file_exists($gambarPath)){
                $gambarUrl = asset("{$directory}/" . $this->gambarbg2);
            }
        }

        return $gambarUrl;
    }

    public function getGambarBgCUrlAttribute($value)
    {
        $gambarUrl = "";

        if(!is_null($this->gambarbg3)){
            $directory = config('cms.image.directory');
            $gambarPath = public_path(). "/{$directory}/" . $this->gambarbg3;
            if(file_exists($gambarPath)){
                $gambarUrl = asset("{$directory}/" . $this->gambarbg3);
            }
        }

        return $gambarUrl;
    }

    public function getDateAttribute($value)
    {
        return is_null($this->created_at) ? '' : $this->created_at->diffForHumans();
    }
}
