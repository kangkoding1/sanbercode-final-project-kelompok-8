<?php
namespace App\Views\Composers;

use Illuminate\View\View;
use App\Category;
use App\Postings;

class NavigationComposer
{
	public function compose(View $view)
	{
		$this->composeCategories($view);

		$this->composePopularPosts($view);
	}

	private function composeCategories(View $view)
	{
		$categories = Category::with(['posts' => function($query) {
            $query->published();
        }])->orderBy('title', 'ASC')->get();

        $view->with('categories', $categories);
	}

	private function composePopularPosts(View $view)
	{
		$popularPosts = Postings::published()->take(3)->get();
        $view->with('popularPosts', $popularPosts);
	}
}