<?php

namespace App\Http\Middleware;

use Closure;

class CheckPermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentUser = $request->user();

        $currentActionName = $request->route()->getActionName();
        list($controller, $method) = explode('@', $currentActionName);
        $controller = str_replace(["App\\Http\\Controllers\\Backend\\", "Controller"], "", $controller);

        $crudPermissionMap = [
            'crud' => ['create','store','edit','update','destroy','restore','forceDestroy','index','view']
        ];

        $classesMap = [
            'postings' => 'postings',
            'categories' => 'category',
            'Users' => 'user',
            'profil' => 'profil',
            'jurusan' => 'jurusan',
            'prestasi' => 'prestasi',
        ];

        foreach ($crudPermissionMap as $permission => $methods) {
            if(in_array($method, $methods) && isset($classesMap[$controller])){
                $className = $classesMap[$controller];

                if(!$currentUser->can("{$permission}-{$className}")){
                    abort(403);
                }
                break;
            }
        }    
            
        return $next($request);
    }
}
