<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Postings;

class UsersController extends BackendController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('name')->get();
        $dump = Postings::select('image')->where('author_id', '=', '1')->get();


        return view("backend.users.index", compact('users', 'dump'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User();
        return view("backend.users.create", compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\UserStoreRequest $request)
    {
        User::create($request->all());

        $notification = array(
            'message' => 'Data berhasil disimpan',
            'alert-type' => 'success'
        );

        return redirect("/backend/users")->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view("backend.users.edit", compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UserUpdateRequest $request, $id)
    {
        User::findOrFail($id)->update($request->all());

        $notification = array(
            'message' => 'Data berhasil diubah',
            'alert-type' => 'success'
        );

        return redirect("/backend/users")->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requests\UserDestroyRequest $request, $id)
    {
        $user = User::findOrFail($id);
        

        $hapusSemua = $request->hapusSemua;

        if($hapusSemua == 'hapusSemua'){
            $dump = Postings::select('image')->where('author_id', '=', $id)->get(); 
            foreach ($dump as $d) {
                $this->removeImage($d->image);   
            }
            
            $user->posts()->withTrashed()->forceDelete();
        }else{
            $user->posts()->update(['author_id' => config("cms.default_user_author_idnya")]);
        }      

        $user->delete();
        
        $notification = array(
            'message' => 'Data berhasil di hapus',
            'alert-type' => 'success'
        );

        return redirect("/backend/users")->with($notification);
    }

    public function removeImage($image)
    {
        if(!empty($image))
        {
            $imagePath = public_path(config('cms.image.directory')) . '/' . $image;
            $ekstensi = substr(strrchr($image, '.'), 1);
            $thumbnail = str_replace(".{$ekstensi}", "_thumbnail.{$ekstensi}", $image);
            $thumbnailPath = public_path(config('cms.image.directory')) . '/' .$thumbnail;

            if(file_exists($imagePath)) unlink($imagePath);
            if(file_exists($thumbnailPath)) unlink($thumbnailPath);
        }
    }
}
