<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Profil;
use Intervention\Image\Facades\Image;

class ProfilController extends BackendController
{
    protected $uploadPath;

    public function __construct()
    {
        parent::__construct();
        $this->uploadPath = public_path(config('cms.image.directory'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ProfilRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profil = Profil::findOrFail($id);

        return view("backend.profil.edit", compact('profil'));
    }

    public function handleRequest($request)
    {
        $data = $request->all();

        if($request->hasFile('gambarbg')){
            $gambarbg = $request->file('gambarbg');
            $fileName = $gambarbg->getClientOriginalName();
            $destination = $this->uploadPath;

            $successUploaded = $gambarbg->move($destination, $fileName);

            $data['gambarbg'] = $fileName;
        }

        if($request->hasFile('logo')){
            $logo = $request->file('logo');
            $fileName = $logo->getClientOriginalName();
            $destination = $this->uploadPath;

            $successUploaded = $logo->move($destination, $fileName);

            $data['logo'] = $fileName;
        }

        if($request->hasFile('gambarbg2')){
            $gambarbg2 = $request->file('gambarbg2');
            $fileName = $gambarbg2->getClientOriginalName();
            $destination = $this->uploadPath;

            $successUploaded = $gambarbg2->move($destination, $fileName);

            $data['gambarbg2'] = $fileName;
        }

        if($request->hasFile('gambarbg3')){
            $gambarbg3 = $request->file('gambarbg3');
            $fileName = $gambarbg3->getClientOriginalName();
            $destination = $this->uploadPath;

            $successUploaded = $gambarbg3->move($destination, $fileName);

            $data['gambarbg3'] = $fileName;
        }

        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\ProfilRequest $request, $id)
    {
        $profil = Profil::findOrFail($id); 
        $gambarLama = $profil->gambarbg;
        $logoLama = $profil->logo;
        $data = $this->handleRequest($request);
        $profil->update($data);

        if($gambarLama !== $profil->gambarbg){
            $this->removeImage($gambarLama);
        }

        if($logoLama !== $profil->logo){
            $this->removeImage($logoLama);
        }

        $notification = array(
            'message' => 'Data berhasil diubah',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function removeImage($image)
    {
        if(!empty($image))
        {
            $imagePath = $this->uploadPath . '/' . $image;

            if(file_exists($imagePath)) unlink($imagePath);
        }
    }
}
