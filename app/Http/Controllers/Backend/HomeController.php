<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\User;
use App\Postings;
use App\Category;
use App\Prestasi;

class HomeController extends BackendController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	$userCount = User::all()->count();
    	$postingsCount = Postings::all()->count();
    	$kategoriCount = Category::all()->count();
    	$prestasiCount = Prestasi::all()->count();

        return view('backend.home', compact('userCount', 'postingsCount', 'kategoriCount', 'prestasiCount'));
    }
}
