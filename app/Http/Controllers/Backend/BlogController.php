<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Postings;
use Intervention\Image\Facades\Image;

class BlogController extends BackendController
{
    protected $uploadPath;

    public function __construct()
    {
        parent::__construct();
        $this->uploadPath = public_path(config('cms.image.directory'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postings = Postings::with('category')->with('author')->latestFirst()->get();

        $postingsTrash = Postings::with('category')->with('author')->latestFirst()->onlyTrashed()->get();

        $postingsPublished = Postings::with('category')->with('author')->latestFirst()->published()->get();

        $postingsDrafted = Postings::with('category')->with('author')->latestFirst()->draft()->get();

        $postingsScheduled = Postings::with('category')->with('author')->latestFirst()->scheduled()->get();

        return view("backend.blog.index", compact('postings', 'postingsTrash','postingsScheduled','postingsDrafted','postingsPublished'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Postings $postings)
    {
        return view("backend.blog.create", compact('postings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\PostRequest $request)
    {
        $data = $this->handleRequest($request);

        $request->user()->posts()->create($data);

        $notification = array(
            'message' => 'Data berhasil disimpan', 
            'alert-type' => 'success'
        );

        return redirect('/backend/blog')->with($notification);
    }

    public function handleRequest($request)
    {
        $data = $request->all();

        if($request->hasFile('image'))
        {
            $width = config('cms.image.thumbnail.width');
            $height = config('cms.image.thumbnail.height');
            $image = $request->file('image');
            $fileName = $image->getClientOriginalName();
            $destination = $this->uploadPath;

            $successUploaded = $image->move($destination, $fileName);

            if($successUploaded)
            {
                $ekstensi = $image->getClientOriginalExtension();
                $thumbnail = str_replace(".{$ekstensi}", "_thumbnail.{$ekstensi}", $fileName);
                Image::make($destination . '/' . $fileName)->resize($width, $height)->save($destination . '/' . $thumbnail);
            }

            $data['image'] = $fileName;
        }

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $postings = Postings::findOrFail($id);
        return view("backend.blog.edit", compact("postings"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\PostRequest $request, $id)
    {
        $postings = Postings::findOrFail($id);
        $gambarLama = $postings->image;
        $data = $this->handleRequest($request);
        $postings->update($data);

        if($gambarLama !== $postings->image){
            $this->removeImage($gambarLama);
        }

        $notification = array(
            'message' => 'Data berhasil diubah', 
            'alert-type' => 'success'
        );

        return redirect('/backend/blog')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Postings::findOrFail($id)->delete();

        $notification = array(
            'message' => $id, 
            'alert-type' => 'trash-message',
        );

        return redirect()->back()->with($notification);
    }

    public function restore($id)
    {
        $postings = Postings::withTrashed()->findOrFail($id);
        $postings->restore();

        $notification = array(
            'message' => 'Data berhasil di kembalikan', 
            'alert-type' => 'success',
        );

        return redirect()->back()->with($notification);
    }

    public function forceDestroy($id)
    {
        $postings = Postings::withTrashed()->findOrFail($id);
        $postings->forceDelete();

        $this->removeImage($postings->image);

        $notification = array(
            'message' => 'Data berhasil di hapus permanen', 
            'alert-type' => 'success',
        );

        return redirect()->back()->with($notification);
    }

    public function removeImage($image)
    {
        if(!empty($image))
        {
            $imagePath = $this->uploadPath . '/' . $image;
            $ekstensi = substr(strrchr($image, '.'), 1);
            $thumbnail = str_replace(".{$ekstensi}", "_thumbnail.{$ekstensi}", $image);
            $thumbnailPath = $this->uploadPath . '/' .$thumbnail;

            if(file_exists($imagePath)) unlink($imagePath);
            if(file_exists($thumbnailPath)) unlink($thumbnailPath);
        }
    }
}
