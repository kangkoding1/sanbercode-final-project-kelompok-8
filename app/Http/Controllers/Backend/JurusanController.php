<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Jurusan;

class JurusanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $daftarjurusan = Jurusan::all();

        return view("backend.jurusan.index", compact('daftarjurusan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $daftarjurusan = new Jurusan();
        return view("backend.jurusan.create", compact('daftarjurusan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\JurusanRequest $request)
    {
        Jurusan::create($request->all());

        $notification = array(
            'message' => 'Data berhasil disimpan',
            'alert-type' => 'success'
        );

        return redirect("/backend/jurusan")->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $daftarjurusan = Jurusan::findOrFail($id);

        return view("backend.jurusan.edit", compact('daftarjurusan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\JurusanRequest $request, $id)
    {
        Jurusan::findOrFail($id)->update($request->all());

        $notification = array(
            'message' => 'Data berhasil diubah',
            'alert-type' => 'success'
        );

        return redirect("/backend/jurusan")->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Jurusan::findOrFail($id)->delete();

        $notification = array(
            'message' => 'Data berhasil di hapus',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
}
