<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Prestasi;
use Intervention\Image\Facades\Image;

class PrestasiController extends BackendController
{
    protected $uploadPath;

    public function __construct()
    {
        parent::__construct();
        $this->uploadPath = public_path(config('cms.image.directory'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prestasi = Prestasi::with('author')->latestFirst()->get();
        $prestasiTrash = Prestasi::with('author')->latestFirst()->onlyTrashed()->get();

        return view("backend.prestasi.index", compact('prestasi','prestasiTrash'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Prestasi $prestasi)
    {
        return view("backend.prestasi.create", compact('prestasi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\PrestasiRequest $request)
    {
        $data = $this->handleRequest($request);

        $request->user()->prestasi()->create($data);

        $notification = array(
            'message' => 'Data berhasil disimpan', 
            'alert-type' => 'success'
        );

        return redirect('/backend/prestasi')->with($notification);
    }

    public function handleRequest($request)
    {
        $data = $request->all();

        if($request->hasFile('image'))
        {
            $width = config('cms.image.thumbnail.width');
            $height = config('cms.image.thumbnail.height');
            $image = $request->file('image');
            $fileName = $image->getClientOriginalName();
            $destination = $this->uploadPath;

            $successUploaded = $image->move($destination, $fileName);

            if($successUploaded)
            {
                $ekstensi = $image->getClientOriginalExtension();
                $thumbnail = str_replace(".{$ekstensi}", "_thumbnail.{$ekstensi}", $fileName);
                Image::make($destination . '/' . $fileName)->resize($width, $height)->save($destination . '/' . $thumbnail);
            }

            $data['image'] = $fileName;
        }

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prestasi = Prestasi::findOrFail($id);
        return view("backend.prestasi.edit", compact("prestasi"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\PrestasiRequest $request, $id)
    {
        $prestasi = Prestasi::findOrFail($id);
        $gambarLama = $prestasi->image;
        $data = $this->handleRequest($request);
        $prestasi->update($data);

        if($gambarLama !== $prestasi->image){
            $this->removeImage($gambarLama);
        }

        $notification = array(
            'message' => 'Data berhasil diubah', 
            'alert-type' => 'success'
        );

        return redirect('/backend/prestasi')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Prestasi::findOrFail($id)->delete();

        $notification = array(
            'message' => $id, 
            'alert-type' => 'trash-message-prestasi',
        );

        return redirect()->back()->with($notification);
    }

    public function restore($id)
    {
        $prestasi = Prestasi::withTrashed()->findOrFail($id);
        $prestasi->restore();

        $notification = array(
            'message' => 'Data berhasil di kembalikan', 
            'alert-type' => 'success',
        );

        return redirect()->back()->with($notification);
    }

    public function forceDestroy($id)
    {
        $prestasi = Prestasi::withTrashed()->findOrFail($id);
        $prestasi->forceDelete();

        $this->removeImage($prestasi->image);

        $notification = array(
            'message' => 'Data berhasil di hapus permanen', 
            'alert-type' => 'success',
        );

        return redirect()->back()->with($notification);
    }

    public function removeImage($image)
    {
        if(!empty($image))
        {
            $imagePath = $this->uploadPath . '/' . $image;
            $ekstensi = substr(strrchr($image, '.'), 1);
            $thumbnail = str_replace(".{$ekstensi}", "_thumbnail.{$ekstensi}", $image);
            $thumbnailPath = $this->uploadPath . '/' .$thumbnail;

            if(file_exists($imagePath)) unlink($imagePath);
            if(file_exists($thumbnailPath)) unlink($thumbnailPath);
        }
    }
}
