<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Postings;
use App\Category;
use App\Http\Requests;
use App\User;
use App\Prestasi;
use App\Jurusan;
use App\Profil;

class BlogController extends Controller
{
	protected $limit = 2;
	

    public function index(){
        $prestasiCount = Prestasi::all()->count();
        $prestasi = Prestasi::all();
        $prestasiNasional = Prestasi::where('tingkat', '=', 'Nasional')->get()->count();
        $prestasiProvinsi = Prestasi::where('tingkat', '=', 'Provinsi')->get()->count();
        $prestasiKabupaten = Prestasi::where('tingkat', '=', 'Kabupaten')->get()->count();
        $daftarjurusan = Jurusan::all();
    	$postings = Postings::with('author')->latestFirst()->published()->simplePaginate($this->limit);
        $postingsCount = Postings::with('author')->latestFirst()->published()->simplePaginate($this->limit)->count();
        $profil = Profil::all();
        $profilHeader = Profil::select('namaheader')->get();
        $profilBg = Profil::select('gambarbg')->first();
        $profilBg2 = Profil::select('gambarbg2')->first();
        $profilBg3 = Profil::select('gambarbg3')->first();

    	return view("blog.index", compact('profilBg','profilBg2','profilBg3','profilHeader','profil','postings','postingsCount','prestasiCount','prestasiNasional','prestasiProvinsi','prestasi','prestasiKabupaten','daftarjurusan'));

    }

    public function kategori(Category $category){
        $postings = $category->posts()->with('author')->latestFirst()->published()->where('category_id', $category->id)->simplePaginate($this->limit);

        $categoryName = $category->title;

        return view("blog.index", compact('postings', 'categoryName'));
    }

    public function tampil(Postings $postings)
    {
        $postings->increment('view_count', 1);
        $profil = Profil::all();
        $profilHeader = Profil::select('namaheader')->get();
        $prestasiCount = Prestasi::all()->count();
        $prestasi = Prestasi::all();
        $prestasiNasional = Prestasi::where('tingkat', '=', 'Nasional')->get()->count();
        $prestasiProvinsi = Prestasi::where('tingkat', '=', 'Provinsi')->get()->count();
        $prestasiKabupaten = Prestasi::where('tingkat', '=', 'Kabupaten')->get()->count();
        $daftarjurusan = Jurusan::all();

    	return view("blog.tampil", compact('profilHeader','profil','postings','prestasiCount','prestasiNasional','prestasiProvinsi','prestasi','prestasiKabupaten','daftarjurusan'));
    }

    public function Author(User $author)
    {
        $postings = $author->posts()->with('category')->latestFirst()->published()->where('author_id', $author->id)->simplePaginate($this->limit);

        $authorName = $author->name;

        return view("blog.index", compact('postings', 'authorName'));
    }

    public function berita()
    {
        $prestasiCount = Prestasi::all()->count();
        $prestasi = Prestasi::all();
        $prestasiNasional = Prestasi::where('tingkat', '=', 'Nasional')->get()->count();
        $prestasiProvinsi = Prestasi::where('tingkat', '=', 'Provinsi')->get()->count();
        $prestasiKabupaten = Prestasi::where('tingkat', '=', 'Kabupaten')->get()->count();
        $daftarjurusan = Jurusan::all();
        $postings = Postings::with('author')->latestFirst()->published()->simplePaginate($this->limit);
        $profil = Profil::all();
        $profilHeader = Profil::select('namaheader')->get();
        $postingsCount = Postings::with('author')->latestFirst()->published()->simplePaginate($this->limit)->count();

        return view("blog.berita", compact('postingsCount','profilHeader','profil','postings','prestasiCount','prestasiNasional','prestasiProvinsi','prestasi','prestasiKabupaten','daftarjurusan'));
    }
}
