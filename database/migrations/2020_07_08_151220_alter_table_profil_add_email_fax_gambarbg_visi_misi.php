<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableProfilAddEmailFaxGambarbgVisiMisi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profil', function (Blueprint $table) {
            $table->string("email");
            $table->string("fax");
            $table->string("gambarbg2");
            $table->string("gambarbg3");
            $table->text("visi");
            $table->text("misi");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profil', function (Blueprint $table) {
            $table->dropColumn("email");
            $table->dropColumn("fax");
            $table->dropColumn("gambarbg2");
            $table->dropColumn("gambarbg3");
            $table->dropColumn("visi");
            $table->dropColumn("misi");
        });
    }
}
