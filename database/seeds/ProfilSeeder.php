<?php

use Illuminate\Database\Seeder;

class ProfilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profil')->insert([
        	[
        		'namasekolah' => 'CMS SCHOOL',
        		'alamat' => 'CMS SCHOOL',
        		'telp' => '000000',
        		'sejarah' => 'CMS SCHOOL',
                'namaheader' => 'CMS'
        	],
        ]);
    }
}
