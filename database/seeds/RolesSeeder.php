<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('roles')->truncate();

        $admin = new Role();
        $admin->name = "admin";
        $admin->display_name = "Administrator";
        $admin->save();

        $editor = new Role();
        $editor->name = "editor";
        $editor->display_name = "Editor";
        $editor->save();

        $author = new Role();
        $author->name = "author";
        $author->display_name = "Author";
        $author->save();

        $u1 = User::find(1);
        $u1->detachRole($admin);
        $u1->attachRole($admin);

        $u2 = User::find(2);
        $u2->detachRole($editor);
        $u2->attachRole($editor);

        $u3 = User::find(3);
        $u3->detachRole($author);
        $u3->attachRole($author);
    }
}
