<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate();

        $faker = Factory::create();

        DB::table('users')->insert([
        	[
        		'name' => "Administrator",
                'slug' => 'administrator',
        		'email' => "admin@cms.com",
        		'password' => bcrypt('admin'),
                'bio' => $faker->text(rand(250, 300)),
                'no_hp' => rand(1,18),
                'alamat' => $faker->text(rand(250, 300)),
        	],
        	[
        		'name' => "Editor Default",
                'slug' => 'editor-default',
        		'email' => "editor@cms.com",
        		'password' => bcrypt('editor'),
                'bio' => $faker->text(rand(250, 300)),
                'no_hp' => rand(1,18),
                'alamat' => $faker->text(rand(250, 300)),
        	],
            [
                'name' => "Author Default",
                'slug' => 'author-default',
                'email' => "author@cms.com",
                'password' => bcrypt('author'),
                'bio' => $faker->text(rand(250, 300)),
                'no_hp' => rand(1,18),
                'alamat' => $faker->text(rand(250, 300)),
            ]
        ]);
    }
}
