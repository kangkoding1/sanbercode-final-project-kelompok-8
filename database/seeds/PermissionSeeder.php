<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('permissions')->truncate();

        $crudPost = new Permission();
        $crudPost->name = "crud-postings";
        $crudPost->save();

        $updateOthersPost = new Permission();
        $updateOthersPost->name = "update-others-postings";
        $updateOthersPost->save();

        $deleteOthersPost = new Permission();
        $deleteOthersPost->name = "delete-others-postings";
        $deleteOthersPost->save();

        $crudCategory = new Permission();
        $crudCategory->name = "crud-category";
        $crudCategory->save();

        $crudUser = new Permission();
        $crudUser->name = "crud-user";
        $crudUser->save();

        $crudProfil = new Permission();
        $crudProfil->name = "crud-profil";
        $crudProfil->save();

        $crudJurusan = new Permission();
        $crudJurusan->name = "crud-jurusan";
        $crudJurusan->save();

        $admin = Role::whereName('admin')->first();
        $editor = Role::whereName('editor')->first();
        $author = Role::whereName('author')->first();

        $admin->detachPermissions([$crudPost, $updateOthersPost, $deleteOthersPost, $crudCategory, $crudUser, $crudProfil, $crudJurusan]);
        $admin->attachPermissions([$crudPost, $updateOthersPost, $deleteOthersPost, $crudCategory, $crudUser, $crudProfil, $crudJurusan]);

        $editor->detachPermissions([$crudPost, $updateOthersPost, $deleteOthersPost, $crudCategory]);
        $editor->attachPermissions([$crudPost, $updateOthersPost, $deleteOthersPost, $crudCategory]);

        $author->detachPermissions([$crudPost]);
        $author->attachPermissions([$crudPost]);
    }
}
