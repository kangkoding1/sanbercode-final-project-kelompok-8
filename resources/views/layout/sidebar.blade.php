<div class="col-lg-4">

  <div class="sidebar">

    <!--<h3 class="sidebar-title">Search</h3>
    <div class="sidebar-item search-form">
      <form action="">
        <input type="text">
        <button type="submit"><i class="icofont-search"></i></button>
      </form>

    </div>--><!-- End sidebar search formn-->

    <h3 class="sidebar-title">Kategori</h3>
    <div class="sidebar-item categories">
      <ul>
        @foreach($categories as $cat)
        <li><a href="{{ route('category', $cat->slug) }}">{{ $cat->title }} <span>{{ $cat->posts->count() }}</span></a></li>
        @endforeach
      </ul>

    </div><!-- End sidebar categories-->

    <h3 class="sidebar-title">Berita Lainya</h3>

    <div class="sidebar-item recent-posts">

      @foreach($popularPosts as $post)
      <div class="post-item clearfix">
        @if($post->image_url)
        <img src="{{ $post->image_url }}" alt="">
        @else
        <img src="{{ asset('frontend/assets/img/berita-bg.jpg') }}" alt="">
        @endif
        <h4><a href="{{ route('blog.tampil', $post->slug) }}">{{ $post->title }}</a></h4>
        <time datetime="{{ $post->date }}">{{ $post->date }}</time>
      </div>
      @endforeach

    </div><!-- End sidebar recent posts-->

  </div><!-- End sidebar -->

</div><!-- End blog sidebar -->