<!DOCTYPE html>
<html  >
<head>
  <!-- Site made with Mobirise Website Builder v4.12.4, mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.12.4, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <meta name="description" content="">
    
  <title>@yield('title', 'SANBERCODE | Sekolah Santuy')</title>
  <link rel="stylesheet" href="{{ asset('frontend/assets/web/assets/mobirise-icons/mobirise-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/bootstrap/css/bootstrap-grid.min.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/bootstrap/css/bootstrap-reboot.min.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/socicon/css/styles.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/dropdown/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/animatecss/animate.min.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/tether/tether.min.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/theme/css/style.css') }}">
  <link rel="preload" as="style" href="{{ asset('frontend/assets/mobirise/css/mbr-additional.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/mobirise/css/mbr-additional.css') }}" type="text/css"> 
  
</head>
<body>
<section class="menu cid-s42m5yqLZm" once="menu" id="menu1-a">
   
    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-toggleable-sm bg-color transparent">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="{{ route('blog') }}">
                        @foreach($profil as $prof)
                        @if($prof->logo != '')
                        <img src="{{ $prof->logo_url }}" alt="" title="" style="height: 5rem;">
                        @else
                        <strong>LOGO</strong>
                        @endif
                        @endforeach
                    </a>
                </span>
                
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                <li class="nav-item">
                    <a class="nav-link link text-white display-4" href="{{ route('blog') }}">
                        <span class="mbri-home mbr-iconfont mbr-iconfont-btn"></span>
                        Home
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link text-white display-4" href="{{ route('blog.berita') }}">
                        <span class="mbri-globe mbr-iconfont mbr-iconfont-btn"></span>
                        Blog
                    </a>
                </li>
            </ul>            
        </div>
    </nav>
</section>

<section class="carousel slide cid-s42BIjOF1N" data-interval="false" id="slider1-f">
    <div class="full-screen">
        <div class="mbr-slider slide carousel" data-keyboard="false" data-ride="carousel" data-interval="3000" data-pause="true">
            <div class="carousel-inner" role="listbox">
                @foreach($profil as $prof)
                @if($prof->gambarbg == '' AND $prof->gambarbg2 == '' AND $prof->gambarbg3 == '')
                <div class="carousel-item slider-fullscreen-image active" data-bg-video-slide="false">
                    <div class="container container-slide">
                        <div class="image_wrapper">
                            <div class="mbr-overlay" style="opacity: 0.4;"></div>
                            <div class="carousel-caption justify-content-center">
                                <div class="col-10 align-left">
                                    <h2 class="mbr-fonts-style display-1"><strong>{{ $prof->namaheader }} *Background Kosong</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                @if($prof->gambarbg != '')
                <div class="carousel-item slider-fullscreen-image active" data-bg-video-slide="false" style="background-image: url('{{ $prof->gambarbga_url }}'); background-size: cover;">
                    <div class="container container-slide">
                        <div class="image_wrapper">
                            <div class="mbr-overlay" style="opacity: 0.4;"></div>
                            <div class="carousel-caption justify-content-center">
                                <div class="col-10 align-left">
                                    <h2 class="mbr-fonts-style display-1"><strong>{{ $prof->namaheader }}</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if($prof->gambarbg2 != '')
                <div class="carousel-item slider-fullscreen-image" data-bg-video-slide="false" style="background-image: url('{{ $prof->gambarbgb_url }}');">
                    <div class="container container-slide">
                        <div class="image_wrapper">
                            <div class="mbr-overlay" style="opacity: 0.4;"></div>
                            <div class="carousel-caption justify-content-center">
                                <div class="col-10 align-left">
                                    <h2 class="mbr-fonts-style display-1"><strong>{{ $prof->namaheader }}</strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if($prof->gambarbg3 != '')
                <div class="carousel-item slider-fullscreen-image" data-bg-video-slide="false" style="background-image: url('{{ $prof->gambarbgc_url }}');">
                    <div class="container container-slide">
                        <div class="image_wrapper">
                            <div class="mbr-overlay" style="opacity: 0.2;"></div>
                            <div class="carousel-caption justify-content-center">
                                <div class="col-10 align-left">
                                    <h2 class="mbr-fonts-style display-1"><strong>{{ $prof->namaheader }}<strong></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @endif
            </div>
            @if($prof->gambarbg2 != '' OR $prof->gambarbg3 != '')
            <a data-app-prevent-settings="" class="carousel-control carousel-control-prev" role="button" data-slide="prev" href="#slider1-f"><span aria-hidden="true" class="mbri-left mbr-iconfont"></span><span class="sr-only">Previous</span></a><a data-app-prevent-settings="" class="carousel-control carousel-control-next" role="button" data-slide="next" href="#slider1-f"><span aria-hidden="true" class="mbri-right mbr-iconfont"></span><span class="sr-only">Next</span></a>
            @endif
            @endforeach
        </div>
    </div>
</section>

<section class="mbr-section info2 cid-s42DsiwwbG" id="info2-i">

    <div class="container">
        <div class="row main justify-content-center">
            <div class="media-container-column col-12 col-lg-3 col-md-4">
                
            </div>
            <div class="media-container-column title col-12 col-lg-7 col-md-6">
                <h2 class="align-right mbr-bold mbr-white pb-3 mbr-fonts-style display-2">
                    MOTTO</h2>
                <h3 class="mbr-section-subtitle align-right mbr-light mbr-white mbr-fonts-style display-2"><em>
                    "{{ $prof->motto != "" ? $prof->motto : "Yaah...masih kosong :(" }}"</em></h3>
            </div>
        </div>
    </div>
</section>

<section class="header12 cid-s42EZLZ5Ah mbr-parallax-background" id="header12-j">

    <div class="mbr-overlay" style="opacity: 0.2; background-color: rgb(35, 35, 35);">
    </div>

    <div class="container  ">
            <div class="media-container">
                <div class="col-md-12 align-center">
                    <h1 class="mbr-section-title pb-3 mbr-white mbr-bold mbr-fonts-style display-1">
                        PIAGAM</h1>
                    <p class="mbr-text pb-3 mbr-white mbr-fonts-style display-5">Beberapa piagam prestasi dari siswa dan siswi</p>
                    

                    <div class="icons-media-container mbr-white">
                        <div class="card col-12 col-md-6 col-lg-4">
                            <div class="icon-block">
                            <a href="#">
                                <span class="mbr-iconfont"><img src="{{ asset('frontend/assets/images/indonesia.png') }}" width="250px"></span>
                            </a>
                            </div>
                            <h5 class="mbr-fonts-style display-5">
                                {{ $prestasiNasional }} PIAGAM</h5>
                            <h5 class="mbr-fonts-style display-5">
                                NASIONAL</h5>
                        </div>

                        <div class="card col-12 col-md-6 col-lg-4">
                            <div class="icon-block">
                                <a href="#">
                                    <span class="mbr-iconfont"><img src="{{ asset('frontend/assets/images/jawatengah.png') }}" width="250px"></span>
                                </a>
                            </div>
                            <h5 class="mbr-fonts-style display-5">
                                {{ $prestasiProvinsi }} PIAGAM</h5>
                            <h5 class="mbr-fonts-style display-5">
                                PROVINSI</h5>
                        </div>

                        <div class="card col-12 col-md-6 col-lg-4">
                            <div class="icon-block">
                                <a href="#">
                                    <span class="mbr-iconfont"><img src="{{ asset('frontend/assets/images/wonosobo.png') }}" width="250px"></span>
                                </a>
                            </div>
                            <h5 class="mbr-fonts-style display-5">
                                {{ $prestasiKabupaten }} PIAGAM</h5>
                            <h5 class="mbr-fonts-style display-5">
                                KABUPATEN</h5>
                        </div>

                        
                    </div>
                </div>
            </div>
    </div>
    
</section>

<section class="accordion1 cid-s43MyqXCQN" id="accordion1-w">

    <div class="container">
        <div class="media-container-row">
            <div class="col-12 col-md-8">
                <div class="section-head text-center space30">
                    <h2 class="mbr-section-title pb-5 mbr-fonts-style display-2"><strong>
                        TENTANG KAMI</strong></h2>
                </div>
                <div class="clearfix"></div>
                <div id="bootstrap-accordion_21" class="panel-group accordionStyles accordion" role="tablist" aria-multiselectable="true">
                    <div class="card">
                        <div class="card-header" role="tab" id="headingOne">
                            <a role="button" class="panel-title collapsed text-black" data-toggle="collapse" data-core="" href="#collapse1_21" aria-expanded="false" aria-controls="collapse1">
                                <h4 class="mbr-fonts-style display-5">
                                    <span class="sign mbr-iconfont mbri-arrow-down inactive"></span>VISI</h4>
                            </a>
                        </div>
                        <div id="collapse1_21" class="panel-collapse noScroll collapse " role="tabpanel" aria-labelledby="headingOne" data-parent="#bootstrap-accordion_21">
                            <div class="panel-body p-4">
                                <p class="mbr-fonts-style panel-text display-7">
                                   {{ $prof->visi != "" ? $prof->visi : "Yaah...masih kosong :(" }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" role="tab" id="headingTwo">
                            <a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-core="" href="#collapse2_21" aria-expanded="false" aria-controls="collapse2">
                                <h4 class="mbr-fonts-style display-5">
                                    <span class="sign mbr-iconfont mbri-arrow-down inactive"></span>MISI</h4>
                            </a>
                            
                        </div>
                        <div id="collapse2_21" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#bootstrap-accordion_21">
                            <div class="panel-body p-4">
                                <p class="mbr-fonts-style panel-text display-7">
                                @if($prof->misi != "")
                                {!! nl2br(e($prof->misi)) !!}
                                @else
                                Yaah...masih kosong :(
                                @endif   
                                </p>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contacts4 cid-s42LvtrPID" id="contacts4-r">

    

    <div class="main_wrapper">
        <div class="b_wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-5 p-0">
                        <div class="block p-5">
                            <div class="block_wrapper">
                                <h4 class="align-left mbr-fonts-style pb-4 m-0 display-5">LOKASI</h4>
                                
                                <p class="mbr-text align-left mbr-fonts-style m-0 display-7">{{ $prof->alamat }}<br><br></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Map-->
        <div class="google-map"><iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15844.010421500681!2d107.5801743!3d-6.89029!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xffa829084444bc68!2ssanbercode!5e0!3m2!1sen!2sid!4v1600519803168!5m2!1sen!2sid" allowfullscreen=""></iframe></div>
    </div>
</section>

<section class="team1 cid-s42GoHt17S" id="team1-l">
    
    
    
    <div class="container align-center">
        <h2 class="pb-3 mbr-fonts-style mbr-section-title display-2"><strong>
            BERITA TERBARU</strong></h2>
        
        <div class="row media-row">
        @if($postingsCount == 0)
        <div class="team-item col-lg-6 col-md-6">
            <div class="item-image">
                <img src="{{ asset('frontend/assets/images/berita-bg-510x287.jpg') }}" alt="" title="">
            </div>
            <div class="item-caption py-3">
                <div class="item-name px-2">
                    <p class="mbr-fonts-style display-5">
                       Kosong</p>
                </div>
                <div class="item-role px-2">
                    <p>Yaah...masih kosong :(</p>
                </div>
                
            </div>
        </div>
        @elseif($postingsCount == 1)
        @foreach($postings as $post)
        <div class="team-item col-lg-6 col-md-6">
            <div class="item-image">
                <img src="{{ asset('frontend/assets/images/berita-bg-510x287.jpg') }}" alt="" title="">
            </div>
            <div class="item-caption py-3">
                <div class="item-name px-2">
                    <p class="mbr-fonts-style display-5">
                       {{ $post->title }}</p>
                </div>
                <div class="item-role px-2">
                    <p>{{ $post->date }}</p>
                </div>
                
            </div>
        </div>
        @endforeach
        @else
        @foreach($postings as $post)
        <div class="team-item col-lg-3 col-md-6">
            <div class="item-image">
                <img src="{{ asset('frontend/assets/images/berita-bg-510x287.jpg') }}" alt="" title="">
            </div>
            <div class="item-caption py-3">
                <div class="item-name px-2">
                    <p class="mbr-fonts-style display-5">
                       {{ $post->title }}</p>
                </div>
                <div class="item-role px-2">
                    <p>{{ $post->date }}</p>
                </div>
                
            </div>
        </div>
        @endforeach
        @endif
        </div>    
    </div>
</section>

<section class="testimonials4 cid-s42Gs1OnZC mbr-parallax-background" id="testimonials4-n">

  

  <div class="mbr-overlay" style="opacity: 0.1; background-color: rgb(35, 35, 35);">
  </div>
  <div class="container">
    <h2 class="pb-3 mbr-fonts-style mbr-white align-center display-2"><strong>PRESTASI TERBARU</strong></h2>
    
    <div class="col-md-10 testimonials-container"> 
      

    @if($prestasiCount == 0)
    <div class="testimonials-item">
        <div class="user row">
          <div class="col-lg-3 col-md-4">
            <div class="user_image">
              <img src="{{ asset('frontend/assets/images/piala-300x300.jpg') }}" alt="" title="">
            </div>
          </div>
          <div class="testimonials-caption col-lg-9 col-md-8">
            <div class="user_text ">
              <p class="mbr-fonts-style  display-7">
                 <em>Yaah...masih kosong :(</em>
              </p>
            </div>
            <div class="user_name mbr-bold mbr-fonts-style align-left pt-3 display-7">
                 Kosong
            </div>
            <div class="user_desk mbr-light mbr-fonts-style align-left pt-2 display-7">
                 Kosong
            </div>
          </div>
        </div>
      </div>
    </div>
    @else
    @foreach($prestasi as $award)
    <div class="testimonials-item">
        <div class="user row">
          <div class="col-lg-3 col-md-4">
            <div class="user_image">
                @if($award->image_url != '')
                <img src="{{ asset('frontend/assets/images/piala-300x300.jpg') }}" alt="" title="">
                @else
                <img src="{{ $award->image_url }}" alt="" title="">
                @endif
            </div>
          </div>
          <div class="testimonials-caption col-lg-9 col-md-8">
            <div class="user_text ">
              <p class="mbr-fonts-style  display-7">
                 <em>{{ $award->title }}</em>
              </p>
            </div>
            <div class="user_name mbr-bold mbr-fonts-style align-left pt-3 display-7">
                 oleh {{ $award->oleh }}
            </div>
            <div class="user_desk mbr-light mbr-fonts-style align-left pt-2 display-7">
                 {{ $award->date }}
            </div>
          </div>
        </div>
      </div>
    </div>
    @endforeach
    @endif
  </div>
</section>

<section class="cid-s43RaeFXRA" id="footer1-10">

    <div class="container">
        <div class="media-container-row content text-white">
            <div class="col-12 col-md-3">
                <div class="media-wrap">
                    <a href="{{ route('blog') }}">
                        @foreach($profil as $prof)
                        @if($prof->logo != '')
                        <img src="{{ $prof->logo_url }}" alt="" title="" style="height: 8rem;">
                        @else
                        <strong>LOGO</strong>
                        @endif
                        @endforeach
                    </a>
                </div>
            </div>
            @foreach($profil as $prof)
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    Sejarah Singkat</h5>
                <p class="mbr-text">
                    {{ $prof->sejarah }}
                </p>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    Kontak</h5>
                <p class="mbr-text">
                    Email: {{ $prof->email != "" ? $prof->email : "-" }}
                    <br>Telp: {{ $prof->telp != "" ? $prof->telp : "-" }}
                    <br>Fax: {{ $prof->fax != "" ? $prof->fax : "-" }}
                </p>
            </div>
            @endforeach
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">Daftar Jurusan</h5>
                <p class="mbr-text">
                    @foreach($daftarjurusan as $jurusan)
                    <a class="text-primary" href="#">{{ $jurusan->nama }}</a>
                    <br><br>
                    @endforeach
                </p>
            </div>
        </div>
        <div class="footer-lower">
            <div class="media-container-row">
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
            <div class="media-container-row mbr-white">
                <div class="col-sm-6 copyright">
                    <p class="mbr-text mbr-fonts-style display-7">
                        © Copyright 2020
                    </p>
                </div>
                <div class="col-md-6">
                    
                </div>
            </div>
        </div>
    </div>
</section>

  <script src="{{ asset('frontend/assets/web/assets/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/popper/popper.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/smoothscroll/smooth-scroll.js') }}"></script>
  <script src="{{ asset('frontend/assets/dropdown/js/nav-dropdown.js') }}"></script>
  <script src="{{ asset('frontend/assets/dropdown/js/navbar-dropdown.js') }}"></script>
  <script src="{{ asset('frontend/assets/touchswipe/jquery.touch-swipe.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/tether/tether.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/ytplayer/jquery.mb.ytplayer.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/vimeoplayer/jquery.mb.vimeo_player.js') }}"></script>
  <script src="{{ asset('frontend/assets/mbr-switch-arrow/mbr-switch-arrow.js') }}"></script>
  <script src="{{ asset('frontend/assets/bootstrapcarouselswipe/bootstrap-carousel-swipe.js') }}"></script>
  <script src="{{ asset('frontend/assets/parallax/jarallax.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/viewportchecker/jquery.viewportchecker.js') }}"></script>
  <script src="{{ asset('frontend/assets/theme/js/script.js') }}"></script>
  <script src="{{ asset('frontend/assets/slidervideo/script.js') }}"></script>
  
  
  <input name="animation" type="hidden">
  </body>
</html>