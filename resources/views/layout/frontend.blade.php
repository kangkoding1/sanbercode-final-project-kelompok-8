<!DOCTYPE html>
<html  >
<head>
  <!-- Site made with Mobirise Website Builder v4.12.4, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.12.4, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <meta name="description" content="Website Generator Description">
  
  
  <title>@yield('title', 'SANBERCODE | Sekolah Santuy')</title>
  <link rel="stylesheet" href="{{ asset('frontend/assets/web/assets/mobirise-icons/mobirise-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/bootstrap/css/bootstrap-grid.min.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/bootstrap/css/bootstrap-reboot.min.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/socicon/css/styles.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/dropdown/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/tether/tether.min.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/animatecss/animate.min.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/theme/css/style.css') }}">
  <link rel="preload" as="style" href="{{ asset('frontend/assets/mobirise/css/mbr-additional.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/assets/mobirise/css/mbr-additional.css') }}" type="text/css">
  
  
  
</head>
<body>
<section class="menu cid-s42m5yqLZm" once="menu" id="menu1-a">
   
    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-toggleable-sm bg-color transparent">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="{{ route('blog') }}">
                        @foreach($profil as $prof)
                        @if($prof->logo != '')
                        <img src="{{ $prof->logo_url }}" alt="" title="" style="height: 8rem;">
                        @else
                        <strong>LOGO</strong>
                        @endif
                        @endforeach
                    </a>
                </span>
                
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                <li class="nav-item">
                    <a class="nav-link link text-white display-4" href="{{ route('blog') }}">
                        <span class="mbri-home mbr-iconfont mbr-iconfont-btn"></span>
                        Home
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link text-white display-4" href="{{ route('blog.berita') }}">
                        <span class="mbri-globe mbr-iconfont mbr-iconfont-btn"></span>
                        Blog
                    </a>
                </li>
            </ul>            
        </div>
    </nav>
</section>

<section class="mbr-section content5 cid-s44mk81tRB mbr-parallax-background" id="content5-12">
   
    <div class="mbr-overlay" style="opacity: 0.2; background-color: rgb(35, 35, 35);">
    </div>

    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-8">
                <h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">
                    BERITA TERBARU
                </h2>                
            </div>
        </div>
    </div>

</section>

@yield('konten')
@yield('kontenblog')
<section class="cid-s43RaeFXRA" id="footer1-10">

    <div class="container">
        <div class="media-container-row content text-white">
            <div class="col-12 col-md-3">
                <div class="media-wrap">
                    <a href="{{ route('blog') }}">
                        @foreach($profil as $prof)
                        @if($prof->logo != '')
                        <img src="{{ $prof->logo_url }}" alt="" title="" style="height: 8rem;">
                        @else
                        <strong>LOGO</strong>
                        @endif
                        @endforeach
                    </a>
                </div>
            </div>
            @foreach($profil as $prof)
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    Sejarah Singkat</h5>
                <p class="mbr-text">
                    {{ $prof->sejarah }}
                </p>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    Kontak</h5>
                <p class="mbr-text">
                    Email: {{ $prof->email != "" ? $prof->email : "-" }}
                    <br>Telp: {{ $prof->telp != "" ? $prof->telp : "-" }}
                    <br>Fax: {{ $prof->fax != "" ? $prof->fax : "-" }}
                </p>
            </div>
            @endforeach
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">Daftar Jurusan</h5>
                <p class="mbr-text">
                    @foreach($daftarjurusan as $jurusan)
                    <a class="text-primary" href="#">{{ $jurusan->nama }}</a>
                    <br><br>
                    @endforeach
                </p>
            </div>
        </div>
        <div class="footer-lower">
            <div class="media-container-row">
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
            <div class="media-container-row mbr-white">
                <div class="col-sm-6 copyright">
                    <p class="mbr-text mbr-fonts-style display-7">
                        © Copyright 2020
                    </p>
                </div>
                <div class="col-md-6">
                    
                </div>
            </div>
        </div>
    </div>
</section>


  <script src="{{ asset('frontend/assets/web/assets/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/popper/popper.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/smoothscroll/smooth-scroll.js') }}"></script>
  <script src="{{ asset('frontend/assets/dropdown/js/nav-dropdown.js') }}"></script>
  <script src="{{ asset('frontend/assets/dropdown/js/navbar-dropdown.js') }}"></script>
  <script src="{{ asset('frontend/assets/tether/tether.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/viewportchecker/jquery.viewportchecker.js') }}"></script>
  <script src="{{ asset('frontend/assets/parallax/jarallax.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/mbr-popup-btns/mbr-popup-btns.js') }}"></script>
  <script src="{{ asset('frontend/assets/touchswipe/jquery.touch-swipe.min.js') }}"></script>
  <script src="{{ asset('frontend/assets/theme/js/script.js') }}"></script>
  
  
  <input name="animation" type="hidden">
  </body>
</html>