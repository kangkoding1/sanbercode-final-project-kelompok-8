<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <title>@yield('title', 'SANBERCODE | Sekolah Santuy')</title>
    <!-- This page CSS -->
    <link href="{{ asset('backend/assets/node_modules/morrisjs/morris.css') }}" rel="stylesheet">
    <!-- Tab CSS -->
    <link href="{{ asset('backend/css/pages/tab-page.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('backend/css/style.min.css') }}" rel="stylesheet">
    <!--alerts CSS -->
    <link href="{{ asset('backend/assets/node_modules/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
    <!-- Dashboard 31 Page CSS -->
    <link href="{{ asset('backend/css/pages/dashboard3.css') }}" rel="stylesheet">
    <!-- DataTable -->
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
    <!-- File Upload CSS -->
    <link href="{{ asset('backend/css/pages/file-upload.css') }}" rel="stylesheet">
    <!-- toast CSS -->
    <link href="{{ asset('backend/assets/node_modules/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/pages/other-pages.css') }}" rel="stylesheet">
    <!-- Dropify -->
    <link rel="stylesheet" href="{{ asset('backend/assets/node_modules/dropify/dist/css/dropify.min.css') }}">
    <!-- Simple MDE -->
    <link rel="stylesheet" href="{{ asset('backend/plugin/simplemde/simplemde.min.css') }}">
    <!-- Custom Simple MDE -->
    <link rel="stylesheet" href="{{ asset('backend/css/custom-simplemde.css') }}">
    <!-- Date picker plugins css -->
    <link href="{{ asset('backend/assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet">
    <!-- Daterange picker plugins css -->
    <link href="{{ asset('backend/assets/node_modules/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/assets/node_modules/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="skin-default-dark fixed-layout">
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        @include('layout.backend.navbar')
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        @include('layout.backend.sidebar')
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        @yield('konten')
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2020
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{ asset('backend/assets/node_modules/jquery/jquery-3.2.1.min.js') }}"></script>
    <!-- Bootstrap popper Core JavaScript -->
    <script src="{{ asset('backend/assets/node_modules/popper/popper.min.js') }}"></script>
    <script src="{{ asset('backend/assets/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('backend/js/perfect-scrollbar.jquery.min.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ asset('backend/js/waves.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('backend/js/sidebarmenu.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('backend/js/custom.min.js') }}"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--sparkline JavaScript -->
    <script src="{{ asset('backend/assets/node_modules/raphael/raphael-min.js') }}"></script>
    <script src="{{ asset('backend/assets/node_modules/morrisjs/morris.js') }}"></script>
    <script src="{{ asset('backend/assets/node_modules/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
    <!-- Vector map JavaScript -->
    <!-- Chart JS -->
    <script src="{{ asset('backend/js/dashboard3.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ asset('backend/assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <script src="{{ asset('backend/assets/node_modules/sparkline/jquery.sparkline.min.js') }}"></script>
    <!-- This is data table -->
    <script src="{{ asset('backend/assets/node_modules/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script src="{{ asset('backend/js/pages/jasny-bootstrap.js') }}"></script>
    <!-- Toast -->
    <script src="{{ asset('backend/assets/node_modules/toast-master/js/jquery.toast.js') }}"></script>
    <script src="{{ asset('backend/js/pages/toastr.js') }}"></script>
    <!-- Dropify -->
    <script src="{{ asset('backend/assets/node_modules/dropify/dist/js/dropify.min.js') }}"></script>
    <!-- Simple MDE -->
    <script src="{{ asset('backend/plugin/simplemde/simplemde.min.js') }}"></script>
    <!-- Plugin JavaScript -->
    <script src="{{ asset('backend/assets/node_modules/moment/moment.js') }}"></script>
    <script src="{{ asset('backend/assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="{{ asset('backend/assets/node_modules/clockpicker/dist/jquery-clockpicker.min.js') }}"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="{{ asset('backend/assets/node_modules/jquery-asColor/dist/jquery-asColor.js') }}"></script>
    <script src="{{ asset('backend/assets/node_modules/jquery-asGradient/dist/jquery-asGradient.js') }}"></script>
    <script src="{{ asset('backend/assets/node_modules/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js') }}"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="{{ asset('backend/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="{{ asset('backend/assets/node_modules/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('backend/assets/node_modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- Sweet-Alert  -->
    <script src="{{ asset('backend/assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('backend/assets/node_modules/sweetalert2/sweet-alert.init.js') }}"></script>
    <script>
    $(function() {
    $('#tableUtamaPostings').DataTable();
    $('#tableTrashPostings').DataTable();
    $('#tableDipublikasiPostings').DataTable();
    $('#tableDraftedPostings').DataTable();
    $('#tableTerjadwalPostings').DataTable();
    $('#tableKategori').DataTable();
    $('#tableUser').DataTable();
    $('#tableJurusan').DataTable();
    $('#tableUtamaPrestasi').DataTable();
    });
    $(document).ready(function() {
        $('.dropify').dropify();
    });

    // MAterial Date picker    
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#tgl-formatee').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });

    $('#tgl').bootstrapMaterialDatePicker();
    </script>
    @yield('jquery')
</body>

</html>