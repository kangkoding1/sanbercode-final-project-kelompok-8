<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User Profile-->
        <div class="user-profile">
            <div class="user-pro-body">
                <div><img src="{{ asset('backend/assets/images/users/user.png') }}" alt="user-img" class="img-circle"></div>
                <div class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle u-dropdown link hide-menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                    <div class="dropdown-menu animated flipInY">
                        <!-- text-->
                        <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i class="fas fa-power-off"></i> Keluar</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                        <!-- text-->
                    </div>
                </div>
            </div>
        </div>
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li> <a href="{{ url('/home') }}"><i class="icon-speedometer"></i><span class="hide-menu">Dashboard </span></a>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-pencil"></i><span class="hide-menu">Blog </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('blog.index') }}">Semua Postingan </a></li>
                        <li><a href="{{ route('blog.create') }}">Tambah Baru</a></li>
                    </ul>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-star"></i><span class="hide-menu">Prestasi </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('prestasi.index') }}">Semua Prestasi </a></li>
                        <li><a href="{{ route('prestasi.create') }}">Tambah Baru</a></li>
                    </ul>
                </li>
                
                @role(['admin','editor'])
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-pin"></i><span class="hide-menu">Kategori </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('categories.index') }}">Daftar Kategori </a></li>
                    </ul>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-wrench"></i><span class="hide-menu">Profil </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('profil.edit', 1) }}">Ubah</a></li>
                    </ul>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-directions"></i><span class="hide-menu">Jurusan </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('jurusan.index') }}">Semua Jurusan </a></li>
                        <li><a href="{{ route('jurusan.create') }}">Tambah Baru</a></li>
                    </ul>
                </li>
                @endrole
                <!--@role('admin')
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-people"></i><span class="hide-menu">User </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('users.index') }}">Daftar User </a></li>
                    </ul>
                </li>
                @endrole-->
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>