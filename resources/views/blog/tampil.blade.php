@extends('layout.frontend')
@section('kontenblog')
<section class="mbr-section content4 cid-s48SdsfXHg" id="content4-1f">

    

    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-8">
                <h2 class="align-center pb-3 mbr-fonts-style display-2">
                    {{ $postings->title }}
                </h2>
                <h3 class="mbr-section-subtitle align-center mbr-light mbr-fonts-style display-5">
                    oleh {{ $postings->author->name }} - {{ $postings->date }}
                </h3>
                
            </div>
        </div>
    </div>
</section>

<section class="mbr-section article content1 cid-s49mHv2kVF" id="content1-1i">
    
     

    <div class="container">
        <div class="media-container-row">
            <div class="mbr-text col-12 mbr-fonts-style display-7 col-md-12">
              <p>
                {!! $postings->body_html !!}
              </p>
            </div>
        </div>
    </div>
</section>
@if($postings->image_url != '')
<section class="cid-s49mMZHicp" id="image1-1m">  
    <figure class="mbr-figure container">
        <div class="image-block" style="width: 66%;">
            <img src="{{ $postings->image_url }}" width="1400" alt="">
            
        </div>
    </figure>
</section>
@endif
@endsection
   

    

   

    
