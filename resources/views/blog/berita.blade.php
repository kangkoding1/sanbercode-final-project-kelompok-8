@extends('layout.frontend')
@section('konten')
@if($postingsCount == 0)
<section class="features18 popup-btn-cards cid-s47SreCp1t" id="features18-17">
    <div class="container">
        <h3 class="mbr-section-subtitle display-5 align-center mbr-fonts-style mbr-light">"Tidak ada kata henti untuk belajar kecuali anda malas"</h3>
        <div class="media-container-row pt-5 ">
            <h2 class="mbr-section-subtitle display-5 align-center mbr-fonts-style mbr-light">Yaah...masih kosong :(</h2>
        </div>
    </div>
</section>
@else
<section class="features18 popup-btn-cards cid-s47SreCp1t" id="features18-17">
    <div class="container">
        <h3 class="mbr-section-subtitle display-5 align-center mbr-fonts-style mbr-light">"Tidak ada kata henti untuk belajar kecuali anda malas"</h3>
        <div class="media-container-row pt-5 ">
            @foreach($postings as $post)
            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="card-wrapper ">
                    <div class="card-img">
                        <div class="mbr-overlay"></div>
                        <div class="mbr-section-btn text-center">
                            <a href="{{ route('blog.tampil', $post->slug) }}" class="btn btn-primary display-4">Selengkapnya</a>
                        </div>
                        @if($post->image_url != '')
                        <img src="{{ $post->image_url }}" alt="">
                        @else
                        <img src="{{ asset('frontend/assets/img/berita-bg.jpg') }}" alt="">
                        @endif
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-7">
                            {{ $post->title }}
                        </h4>
                        <br>
                        {{ $post->date }}
                        <p class="mbr-text mbr-fonts-style align-left display-7">
                            {{ $post->excerpt }}
                            <br>
                            <br>
                            oleh {{ $post->author->name }}
                        </p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endif
@endsection