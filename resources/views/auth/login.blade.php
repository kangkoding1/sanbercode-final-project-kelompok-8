@extends('layout.app')

@section('konten')
<div class="login-register" style="background-image:url(backend/assets/images/background/login-register.jpg);">
    <div class="login-box card">
        <div class="card-body">
            <form class="form-horizontal form-material" id="loginform" method="POST" action="{{ route('login') }}">
                @csrf
                <h3 class="text-center m-b-20">Admin CMS</h3>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" placeholder="Username" name="email"> </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" required="" placeholder="Password" name="password"> </div>
                </div>
                <div class="form-group text-center">
                    <div class="col-xs-12 p-b-20">
                        <button class="btn btn-block btn-lg btn-info btn-rounded" type="submit">Masuk</button>
                    </div>
                    <h5 class="text-center m-b-20">by <a href="#"> Kelompok 8</h5>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
