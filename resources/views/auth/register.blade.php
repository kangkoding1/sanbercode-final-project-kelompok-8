<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('backend/assets/images/favicon.png') }}">
    <title>@yield('title', 'SMAKANZA | SMK Negeri 1 Wonosobo')</title>
    <!-- page css -->
    <link href="{{ asset('backend/assets/node_modules/register-steps/steps.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/pages/register3.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('backend/css/style.min.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="skin-default card-no-border" style="background-image:url(backend/assets/images/background/login-register.jpg);">
    <section id="wrapper" class="step-register">
        <div class="register-box">
            <div class="">
                <a href="javascript:void(0)" class="text-center m-b-40"><img width="100px" height="100px" src="{{ asset('backend/assets/images/logo_transparent.png') }}" alt="homepage" class="dark-logo" /></a>
                <!-- multistep form -->
                <form id="msform">
                    <!-- progressbar -->
                    <ul id="eliteregister">
                        <li class="active"></li>
                        <li></li>
                        <li></li>
                    </ul>
                    <!-- fieldsets -->
                    <fieldset>
                        <h2 class="fs-title">ISI DATA AKUN</h2>
                        <h3 class="fs-subtitle">Pastikan Data Lengkap!</h3>
                        <input type="text" name="email" placeholder="Email" />
                        <input type="password" name="pass" placeholder="Password" />
                        <input type="button" name="next" class="next action-button" value="Next" />
                    </fieldset>
                    <fieldset>
                        <h2 class="fs-title">ISI DATA DIRI</h2>
                        <h3 class="fs-subtitle">Lengkapi Profil Singkat!</h3>
                        <input type="text" name="name" placeholder="Nama" />
                        <input type="text" name="no_hp" placeholder="Nomor Handphone" />
                        <textarea name="alamat" placeholder="Alamat"></textarea>
                        <input type="button" name="previous" class="previous action-button" value="Previous" />
                        <input type="button" name="next" class="next action-button" value="Next" />
                    </fieldset>
                    <fieldset>
                        <h2 class="fs-title">ROLE AKUN</h2>
                        <h3 class="fs-subtitle">Tentukan Role Akun!</h3>
                        <select name="role" class="form-control custom-select">
                            <option>-- Pilih Role --</option>
                            <option>Admin</option>
                            <option>Author</option>
                        </select>
                        <input type="button" name="previous" class="previous action-button" value="Previous" />
                        <input type="submit" name="submit" class="submit action-button" value="Submit" />
                    </fieldset>
                </form>
                <div class="clear"></div>
            </div>
        </div>
    </section>
<script src="{{ asset('backend/assets/node_modules/jquery/jquery-3.2.1.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('backend/assets/node_modules/popper/popper.min.js') }}"></script>
    <script src="{{ asset('backend/assets/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="{{ asset('backend/assets/node_modules/register-steps/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('backend/assets/node_modules/register-steps/register-init.js') }}"></script>
    <script type="text/javascript">
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    });
    // ============================================================== 
    // Login and Recover Password 
    // ============================================================== 
    $('#to-recover').on("click", function() {
        $("#loginform").slideUp();
        $("#recoverform").fadeIn();
    });
    </script>
</body>

</html>