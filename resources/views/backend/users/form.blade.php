<form class="form-material m-t-40">
    <div class="form-group {{ $errors->has('name') ? 'has-danger' : '' }}">
        {!! Form::label('name', 'Nama') !!}
        {!! Form::text('name', null, ['class' => 'form-control form-control-line']) !!}

        @if($errors->has('name'))
            <small class="form-control-feedback">{{ $errors->first('name') }}</small>
        @endif
    </div>

    <div class="form-group {{ $errors->has('email') ? 'has-danger' : '' }}">
        {!! Form::label('email', 'Email') !!}
        {!! Form::text('email', null, ['class' => 'form-control form-control-line']) !!}

        @if($errors->has('email'))
            <small class="form-control-feedback">{{ $errors->first('email') }}</small>
        @endif
    </div>

    <div class="form-group {{ $errors->has('password') ? 'has-danger' : '' }}">
        {!! Form::label('password', 'Password') !!}
        {!! Form::password('password', ['class' => 'form-control form-control-line']) !!}

        @if($errors->has('password'))
            <small class="form-control-feedback">{{ $errors->first('password') }}</small>
        @endif
    </div>

    {!! Form::button('<i class="icon-check"></i> Simpan', ['type' => 'submit', 'class' => 'btn waves-effect waves-light btn-outline-success'] )  !!}
</form>