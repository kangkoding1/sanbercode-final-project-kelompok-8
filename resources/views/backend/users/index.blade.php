@extends('layout.backend.utama')

@section('konten')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">User</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">User</a></li>
                        <li class="breadcrumb-item active">Daftar User</li>
                    </ol>
                    <!--<a href="{{ route('users.create') }}" type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="icon-plus"></i> Tambah Data</a>-->
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Data</h4>
                    <h6 class="card-subtitle">Daftar semua data User.</code></h6>
                    <div class="table-responsive">
                        <table id="tableUser" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>No HP</th>
                                    <th>Alamat</th>
                                    <th>Biodata</th>
                                    <th>Role</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->no_hp }}</td>
                                    <td>{{ $user->alamat }}</td>
                                    <td>{{ $user->bio }}</td>
                                    <td>Role</td>
                                    <td>
                                        @if($user->id != 1 && $user->id != 2)

                                        <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#confirmModal" data-whatever="@mdo"><i class="icon-trash"></i> Hapus</button>
                                        <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel1">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title text-danger" id="confirmModalLabel1">KONFIRMASI</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h4 class="text-danger">Apakah anda yakin akan menghapus User tersebut?</h4>
                                                        <br>
                                                        {!! Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $user->id]]) !!}

                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="hapusSemua" name="hapusSemua" value="hapusSemua">
                                                            <label class="custom-control-label text-danger" for="hapusSemua">Hapus beserta Blog yang terkait dengan User tersebut</label>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-danger">Ya</button>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        <a href="{{ route('users.edit', $user->id) }}" type="button" class="btn btn-outline-info"><i class="icon-note"></i> Edit</a>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
    @foreach($users as $user)
    <div class="col-lg-4 col-md-6">
        <!-- Card -->
        <div class="card" style="padding: 50px 50px 50px 50px;">
            <img class="card-img-top img-responsive" src="{{ asset('backend/assets/images/users/user.png') }}" alt="Card image cap">
            <div class="card-body">
                <h4 class="card-title">{{ $user->name }}</h4>
                <p class="card-text"><i class="icon-envelope-open"></i> {{ $user->email }}</p>
                <p class="card-text"><i class="icon-phone"></i> {{ $user->no_hp }}</p>
                <p class="card-text"><i class="icon-location-pin"></i> {{ $user->alamat }}</p>
                <p class="card-text"><i class="icon-notebook"></i> {{ $user->bio }}</p>
                <a href="{{ route('users.edit', $user->id) }}" type="button" class="btn btn-outline-info"><i class="icon-note"></i> Edit</a>
            </div>
        </div>
        <!-- Card -->
    </div>
    @endforeach
</div>
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
</div>
@endsection

@include('backend.partials.message')