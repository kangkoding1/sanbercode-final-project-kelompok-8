<form class="form-material m-t-40">
    <div class="form-group {{ $errors->has('namasekolah') ? 'has-danger' : '' }}">
        {!! Form::label('namasekolah', 'Nama Sekolah') !!}
        {!! Form::text('namasekolah', null, ['class' => 'form-control form-control-line']) !!}

        @if($errors->has('namasekolah'))
            <small class="form-control-feedback">{{ $errors->first('namasekolah') }}</small>
        @endif
    </div>

    <div class="form-group {{ $errors->has('namaheader') ? 'has-danger' : '' }}">
        {!! Form::label('namaheader', 'Nama Header (Nama Julukan Sekolah)') !!}
        {!! Form::text('namaheader', null, ['class' => 'form-control form-control-line']) !!}

        @if($errors->has('namaheader'))
            <small class="form-control-feedback">{{ $errors->first('namaheader') }}</small>
        @endif
    </div>

    <div class="form-group {{ $errors->has('alamat') ? 'has-danger' : '' }}">
        {!! Form::label('alamat', 'Alamat') !!}
        {!! Form::text('alamat', null, ['class' => 'form-control form-control-line']) !!}

        @if($errors->has('alamat'))
            <small class="form-control-feedback">{{ $errors->first('alamat') }}</small>
        @endif
    </div>

    <div class="form-group {{ $errors->has('telp') ? 'has-danger' : '' }}">
        {!! Form::label('telp', 'Telp') !!}
        {!! Form::text('telp', null, ['class' => 'form-control form-control-line']) !!}

        @if($errors->has('telp'))
            <small class="form-control-feedback">{{ $errors->first('telp') }}</small>
        @endif
    </div>

    <div class="form-group {{ $errors->has('fax') ? 'has-danger' : '' }}">
        {!! Form::label('fax', 'Fax') !!}
        {!! Form::text('fax', null, ['class' => 'form-control form-control-line']) !!}

        @if($errors->has('fax'))
            <small class="form-control-feedback">{{ $errors->first('fax') }}</small>
        @endif
    </div>

    <div class="form-group {{ $errors->has('motto') ? 'has-danger' : '' }}">
        {!! Form::label('motto', 'Motto') !!}
        {!! Form::text('motto', null, ['class' => 'form-control form-control-line']) !!}

        @if($errors->has('motto'))
            <small class="form-control-feedback">{{ $errors->first('motto') }}</small>
        @endif
    </div>

    <div class="form-group {{ $errors->has('visi') ? 'has-danger' : '' }}">
        {!! Form::label('visi', 'Visi') !!}
        {!! Form::text('visi', null, ['class' => 'form-control form-control-line']) !!}

        @if($errors->has('visi'))
            <small class="form-control-feedback">{{ $errors->first('visi') }}</small>
        @endif
    </div>

    <div class="form-group {{ $errors->has('misi') ? 'has-danger' : '' }}">
        {!! Form::label('misi', 'Misi') !!}
        {!! Form::textarea('misi', null, ['rows' => 10, 'class' => 'form-control form-control-line']) !!}

        @if($errors->has('misi'))
            <small class="form-control-feedback">{{ $errors->first('misi') }}</small>
        @endif
    </div>

    <div class="form-group {{ $errors->has('sejarah') ? 'has-danger' : '' }}">
        {!! Form::label('sejarah', 'Sejarah') !!}
        {!! Form::text('sejarah', null, ['class' => 'form-control form-control-line']) !!}

        @if($errors->has('sejarah'))
            <small class="form-control-feedback">{{ $errors->first('sejarah') }}</small>
        @endif
    </div>

    <div class="form-group {{ $errors->has('gambarbg') ? 'has-danger' : '' }}">
        {!! Form::label('gambarbg', 'Gambar Background') !!}
        {!! Form::file('gambarbg', ['class' => 'dropify', 'data-default-file' => "{{ ($profil->gambarbg) ? $profil->gambarbg : '' }}"]) !!}

         @if($errors->has('gambarbg'))
            <small class="form-control-feedback">{{ $errors->first('gambarbg') }}</small>
        @endif
    </div>

     <div class="form-group {{ $errors->has('gambarbg2') ? 'has-danger' : '' }}">
        {!! Form::label('gambarbg2', 'Gambar Background (Opsional)') !!}
        {!! Form::file('gambarbg2', ['class' => 'dropify', 'data-default-file' => "{{ ($profil->gambarbg2) ? $profil->gambarbg2 : '' }}"]) !!}

         @if($errors->has('gambarbg2'))
            <small class="form-control-feedback">{{ $errors->first('gambarbg2') }}</small>
        @endif
    </div>

     <div class="form-group {{ $errors->has('gambarbg3') ? 'has-danger' : '' }}">
        {!! Form::label('gambarbg3', 'Gambar Background (Opsional)') !!}
        {!! Form::file('gambarbg3', ['class' => 'dropify', 'data-default-file' => "{{ ($profil->gambarbg3) ? $profil->gambarbg3 : '' }}"]) !!}

         @if($errors->has('gambarbg3'))
            <small class="form-control-feedback">{{ $errors->first('gambarbg3') }}</small>
        @endif
    </div>

    <div class="form-group {{ $errors->has('logo') ? 'has-danger' : '' }}">
        {!! Form::label('logo', 'Logo') !!}
        {!! Form::file('logo', ['class' => 'dropify', 'data-default-file' => "{{ ($profil->logo) ? $profil->logo : '' }}"]) !!}

         @if($errors->has('logo'))
            <small class="form-control-feedback">{{ $errors->first('logo') }}</small>
        @endif
    </div>

    {!! Form::button('<i class="icon-check"></i> Simpan', ['type' => 'submit', 'class' => 'btn waves-effect waves-light btn-outline-success'] )  !!}
</form>