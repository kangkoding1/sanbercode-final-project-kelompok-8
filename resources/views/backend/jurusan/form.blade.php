<form class="form-material m-t-40">
    <div class="form-group {{ $errors->has('nama') ? 'has-danger' : '' }}">
        {!! Form::label('nama', 'Nama Jurusan') !!}
        {!! Form::text('nama', null, ['class' => 'form-control form-control-line']) !!}

        @if($errors->has('nama'))
            <small class="form-control-feedback">{{ $errors->first('nama') }}</small>
        @endif
    </div>

    {!! Form::button('<i class="icon-check"></i> Simpan', ['type' => 'submit', 'class' => 'btn waves-effect waves-light btn-outline-success'] )  !!}
</form>