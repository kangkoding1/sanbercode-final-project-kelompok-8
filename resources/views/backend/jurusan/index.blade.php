@extends('layout.backend.utama')

@section('konten')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Jurusan</h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Jurusan</a></li>
                            <li class="breadcrumb-item active">Daftar Jurusan</li>
                        </ol>
                        <a href="{{ route('jurusan.create') }}" type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="icon-plus"></i> Tambah Data</a>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Data</h4>
                            <h6 class="card-subtitle">Daftar semua data Jurusan.</code></h6>
                            <div class="table-responsive">
                                <table id="tableJurusan" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Tanggal</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($daftarjurusan as $jurusan)
                                            <tr>
                                                <td>{{ $jurusan->nama }}</td>
                                                <td>{{ $jurusan->created_at }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#confirmModal" data-whatever="@mdo"><i class="icon-trash"></i> Hapus</button>
                                                    <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel1">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title text-danger" id="confirmModalLabel1">KONFIRMASI</h4>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <h4 class="text-danger">Apakah anda yakin akan menghapus Jurusan tersebut?</h4>
                                                                    <br>
                                                                    {!! Form::open(['method' => 'DELETE', 'route' => ['jurusan.destroy', $jurusan->id]]) !!}
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-danger">Ya</button>
                                                                </div>
                                                                    {!! Form::close() !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="{{ route('jurusan.edit', $jurusan->id) }}" type="button" class="btn btn-outline-info"><i class="icon-note"></i> Edit</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
    </div>
@endsection

@include('backend.partials.message')
