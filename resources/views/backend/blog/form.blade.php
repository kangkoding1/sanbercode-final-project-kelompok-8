<form class="form-material m-t-40">
    <div class="form-group {{ $errors->has('title') ? 'has-danger' : '' }}">
        {!! Form::label('title', 'Judul') !!}
        {!! Form::text('title', null, ['class' => 'form-control form-control-line']) !!}

        @if($errors->has('title'))
            <small class="form-control-feedback">{{ $errors->first('title') }}</small>
        @endif
    </div>

    <div class="form-group {{ $errors->has('slug') ? 'has-danger' : '' }}">
        {!! Form::label('slug', 'Slug') !!}
        {!! Form::text('slug', null, ['class' => 'form-control form-control-line']) !!}

        @if($errors->has('slug'))
            <small class="form-control-feedback">{{ $errors->first('slug') }}</small>
        @endif
    </div>

    <div class="form-group excerpt">
        {!! Form::label('excerpt', 'Ringkasan') !!}
        {!! Form::text('excerpt', null, ['class' => 'form-control form-control-line']) !!}
    </div>

    <div class="form-group {{ $errors->has('body') ? 'has-danger' : '' }}">
        {!! Form::label('body', 'Isi') !!}
        {!! Form::text('body', null, ['class' => 'form-control form-control-line']) !!}

        @if($errors->has('body'))
            <small class="form-control-feedback">{{ $errors->first('body') }}</small>
        @endif
    </div>

    <div class="form-group">
        {!! Form::label('published_at', 'Tanggal Publikasi') !!}
        {!! Form::text('published_at', null, ['class' => 'form-control', 'id' => 'tgl']) !!}
    </div>

    <div class="form-group {{ $errors->has('category_id') ? 'has-danger' : '' }}">
        {!! Form::label('category_id', 'Kategori') !!}
        {!! Form::select('category_id', App\Category::pluck('title', 'id'), null, ['class' => 'form-control form-control-line',
         'placeholder' => 'Pilih Kategori']) !!}

         @if($errors->has('category_id'))
            <small class="form-control-feedback">{{ $errors->first('category_id') }}</small>
        @endif
    </div>

    <div class="form-group {{ $errors->has('image') ? 'has-danger' : '' }}">
        {!! Form::label('image', 'Gambar') !!}
        {!! Form::file('image', ['class' => 'dropify', 'data-default-file' => "{{ ($postings->image_thumb_url) ? $postings->image_thumb_url : '' }}"]) !!}

         @if($errors->has('image'))
            <small class="form-control-feedback">{{ $errors->first('image') }}</small>
        @endif
    </div>

    {!! Form::button('<i class="icon-check"></i> Simpan', ['type' => 'submit', 'class' => 'btn waves-effect waves-light btn-outline-success'] )  !!}
</form>