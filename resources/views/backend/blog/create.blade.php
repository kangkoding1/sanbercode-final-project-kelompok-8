@extends('layout.backend.utama')

@section('konten')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Blog</h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Blog</a></li>
                            <li class="breadcrumb-item active">Tambah Baru</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row">
	            <div class="col-12">
	            	<div class="card">
                	    <div class="card-body">
                	        <h4 class="card-title">Data</h4>
                	        <h6 class="card-subtitle">Tambah untuk data Blog.</h6>
                	        {!! Form::model($postings, [
                                    'method' => 'POST',
                                    'route' => 'blog.store',
                                    'class' => 'form-material m-t-40',
                                    'files' => TRUE
                                ])
                            !!}

                            @include('backend.blog.form')
                            
                            {!! Form::close() !!}
                	    </div>
	            	</div>
	            </div>
        	</div>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
    </div>
@endsection

@include('backend.blog.script')