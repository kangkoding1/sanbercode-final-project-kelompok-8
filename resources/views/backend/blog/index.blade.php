@extends('layout.backend.utama')

@section('konten')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Blog</h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Blog</a></li>
                            <li class="breadcrumb-item active">Semua Postingan</li>
                        </ol>
                        <a href="{{ route('blog.create') }}" type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="icon-plus"></i> Tambah Data</a>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Data</h4>
                            <h6 class="card-subtitle">Daftar semua data Blog.</code></h6>
                            <!-- Nav tabs -->
                            <div class="vtabs customvtab">
                                <ul class="nav nav-tabs tabs-vertical" role="tablist">
                                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#utama" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Utama</span> </a> </li>

                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#dipublikasi" role="tab"><span class="hidden-sm-up"><i class="icon-plane"></i></span> <span class="hidden-xs-down">Dipublikasi</span></a> </li>

                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#terjadwal" role="tab"><span class="hidden-sm-up"><i class="icon-clock"></i></span> <span class="hidden-xs-down">Terjadwal</span></a> </li>

                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#drafted" role="tab"><span class="hidden-sm-up"><i class="icon-puzzle"></i></span> <span class="hidden-xs-down">Draft</span></a> </li>

                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#tempatsampah" role="tab"><span class="hidden-sm-up"><i class="icon-trash"></i></span> <span class="hidden-xs-down">Tempat Sampah</span></a> </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content" style="width: 100%;">
                                    <div class="tab-pane active" id="utama" role="tabpanel">
                                        <div class="table-responsive">
                                            <table id="tableUtamaPostings" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Judul</th>
                                                        <th>Author</th>
                                                        <th>Kategori</th>
                                                        <th>Tanggal</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($postings as $post)
                                                        <tr>
                                                            <td>{{ $post->title }}</td>
                                                            <td>{{ $post->author->name }}</td>
                                                            <td>{{ $post->category->title }}</td>
                                                            <td>
                                                                <abbr title="{{ $post->dateFormatted(true) }}"> {{ $post->dateFormatted() }}
                                                                </abbr>
                                                                &nbsp;&nbsp;
                                                                {!! $post->publicationLabel() !!}
                                                            </td>
                                                            <td>
                                                                <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#confirmModal" data-whatever="@mdo"><i class="icon-trash"></i> Hapus</button>
                                                                <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel1">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title text-danger" id="confirmModalLabel1">KONFIRMASI</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <h4 class="text-danger">Apakah anda yakin akan menghapus Blog tersebut?</h4>
                                                                                <br>
                                                                                {!! Form::open(['method' => 'DELETE', 'route' => ['blog.destroy', $post->id]]) !!}
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit" class="btn btn-danger">Ya</button>
                                                                            </div>
                                                                                {!! Form::close() !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <a href="{{ route('blog.edit', $post->id) }}" type="button" class="btn btn-outline-info"><i class="icon-note"></i> Edit</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="dipublikasi" role="tabpanel">
                                        <div class="table-responsive">
                                            <table id="tableDipublikasiPostings" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Judul</th>
                                                        <th>Author</th>
                                                        <th>Kategori</th>
                                                        <th>Tanggal</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($postingsPublished as $postPublished)
                                                        <tr>
                                                            <td>{{ $postPublished->title }}</td>
                                                            <td>{{ $postPublished->author->name }}</td>
                                                            <td>{{ $postPublished->category->title }}</td>
                                                            <td>
                                                                <abbr title="{{ $postPublished->dateFormatted(true) }}"> {{ $postPublished->dateFormatted() }}
                                                                </abbr>
                                                                &nbsp;&nbsp;
                                                                {!! $postPublished->publicationLabel() !!}
                                                            </td>
                                                            <td>
                                                                <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#confirmModal" data-whatever="@mdo"><i class="icon-trash"></i> Hapus</button>
                                                                <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel1">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title text-danger" id="confirmModalLabel1">KONFIRMASI</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <h4 class="text-danger">Apakah anda yakin akan menghapus Blog tersebut?</h4>
                                                                                <br>
                                                                                {!! Form::open(['method' => 'DELETE', 'route' => ['blog.destroy', $postPublished->id]]) !!}
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit" class="btn btn-danger">Ya</button>
                                                                            </div>
                                                                                {!! Form::close() !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <a href="{{ route('blog.edit', $postPublished->id) }}" type="button" class="btn btn-outline-info"><i class="icon-note"></i> Edit</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="terjadwal" role="tabpanel">
                                        <div class="table-responsive">
                                            <table id="tableTerjadwalPostings" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Judul</th>
                                                        <th>Author</th>
                                                        <th>Kategori</th>
                                                        <th>Tanggal</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($postingsScheduled as $postScheduled)
                                                        <tr>
                                                            <td>{{ $postScheduled->title }}</td>
                                                            <td>{{ $postScheduled->author->name }}</td>
                                                            <td>{{ $postScheduled->category->title }}</td>
                                                            <td>
                                                                <abbr title="{{ $postScheduled->dateFormatted(true) }}"> {{ $postScheduled->dateFormatted() }}
                                                                </abbr>
                                                                &nbsp;&nbsp;
                                                                {!! $postScheduled->publicationLabel() !!}
                                                            </td>
                                                            <td>
                                                                <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#confirmModal" data-whatever="@mdo"><i class="icon-trash"></i> Hapus</button>
                                                                <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel1">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title text-danger" id="confirmModalLabel1">KONFIRMASI</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <h4 class="text-danger">Apakah anda yakin akan menghapus Blog tersebut?</h4>
                                                                                <br>
                                                                                {!! Form::open(['method' => 'DELETE', 'route' => ['blog.destroy', $postScheduled->id]]) !!}
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit" class="btn btn-danger">Ya</button>
                                                                            </div>
                                                                                {!! Form::close() !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <a href="{{ route('blog.edit', $postScheduled->id) }}" type="button" class="btn btn-outline-info"><i class="icon-note"></i> Edit</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="drafted" role="tabpanel">
                                        <div class="table-responsive">
                                            <table id="tableDraftedPostings" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Judul</th>
                                                        <th>Author</th>
                                                        <th>Kategori</th>
                                                        <th>Tanggal</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($postingsDrafted as $postDrafted)
                                                        <tr>
                                                            <td>{{ $postDrafted->title }}</td>
                                                            <td>{{ $postDrafted->author->name }}</td>
                                                            <td>{{ $postDrafted->category->title }}</td>
                                                            <td>
                                                                <abbr title="{{ $postDrafted->dateFormatted(true) }}"> {{ $postDrafted->dateFormatted() }}
                                                                </abbr>
                                                                &nbsp;&nbsp;
                                                                {!! $postDrafted->publicationLabel() !!}
                                                            </td>
                                                            <td>
                                                                <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#confirmModal" data-whatever="@mdo"><i class="icon-trash"></i> Hapus</button>
                                                                <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel1">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title text-danger" id="confirmModalLabel1">KONFIRMASI</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <h4 class="text-danger">Apakah anda yakin akan menghapus Blog tersebut?</h4>
                                                                                <br>
                                                                                {!! Form::open(['method' => 'DELETE', 'route' => ['blog.destroy', $postDrafted->id]]) !!}
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit" class="btn btn-danger">Ya</button>
                                                                            </div>
                                                                                {!! Form::close() !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <a href="{{ route('blog.edit', $postDrafted->id) }}" type="button" class="btn btn-outline-info"><i class="icon-note"></i> Edit</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="tempatsampah" role="tabpanel">
                                        <div class="table-responsive">
                                            <table id="tableTrashPostings" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Judul</th>
                                                        <th>Author</th>
                                                        <th>Kategori</th>
                                                        <th>Tanggal</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($postingsTrash as $posts)
                                                        <tr>
                                                            <td>{{ $posts->title }}</td>
                                                            <td>{{ $posts->author->name }}</td>
                                                            <td>{{ $posts->category->title }}</td>
                                                            <td>
                                                                <abbr title="{{ $posts->dateFormatted(true) }}"> {{ $posts->dateFormatted() }}
                                                                </abbr>
                                                            </td>
                                                            <td>
                                                        {!! Form::open(['method' => 'PUT', 'route' => ['backend.blog.restore', $posts->id]]) !!}

                                                                <button type="submit" class="btn btn-outline-primary"><i class="icon-reload"></i> Kembalikan</button>

                                                        {!! Form::close() !!}
                                                        &nbsp;
                                                        {!! Form::open(['method' => 'DELETE', 'route' => ['backend.blog.force-destroy', $posts->id]]) !!}

                                                                <button type="submit" class="btn btn-outline-danger"><i class="icon-close" onclick="return confirm('Anda yakin akan menghapus permanen?')"></i> Hapus Permanen</button>

                                                        {!! Form::close() !!}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
    </div>
@endsection

@include('backend.partials.message')
