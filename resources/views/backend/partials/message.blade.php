@section('jquery')
@if (Session::has('message'))
<script type="text/javascript">
    var type = "{{ Session::get('alert-type') }}";
    switch(type){

    case 'info':
        $.toast({
          heading: 'Informasi',
          text: "{{Session::get('message')}}",
          loaderBg:'#ff6849',
          icon: 'info',
          hideAfter: 3000, 
          stack: 6,
          showHideTransition: 'slide',
          position: 'top-center',
          stack: false
        });
    break;

    case 'success':
        $.toast({
          heading: 'Berhasil',
          text: "{{Session::get('message')}}",
          loaderBg:'#ff6849',
          icon: 'success',
          hideAfter: 3000, 
          stack: 6,
          showHideTransition: 'slide',
          position: 'top-center',
          stack: false,
        });
    break;

    case 'warning':
        $.toast({
          heading: 'Peringatan',
          text: "{{Session::get('message')}}",
          loaderBg:'#ff6849',
          icon: 'warning',
          hideAfter: 3000, 
          stack: 6,
          showHideTransition: 'slide',
          position: 'top-center',
          stack: false,
          textColor : '#ecf0f1',
          bgColor : '#f39c12'
        });
    break;

    case 'error':
        $.toast({
          heading: 'Kesalahan',
          text: "{{Session::get('message')}}",
          loaderBg:'#ff6849',
          icon: 'error',
          hideAfter: 3000, 
          stack: 6,
          showHideTransition: 'slide',
          position: 'top-center',
          stack: false
        });               
    break;

    case 'trash-message':
        Swal.fire({
                  type: 'info',
                  title: 'Informasi',
                  text: 'Data dipindahkan ke tempat sampah.',
                  footer : '{!! Form::open(['method' => 'PUT', 'route' => ['backend.blog.restore', Session::get('message') ]]) !!} {!! Form::button('Klik untuk UNDO', ['type' => 'submit', 'class' => 'btn waves-effect waves-light btn-outline-warning'] )  !!} {!! Form::close() !!}'
              });
    break;

    case 'trash-message-prestasi':
        Swal.fire({
                  type: 'info',
                  title: 'Informasi',
                  text: 'Data dipindahkan ke tempat sampah.',
                  footer : '{!! Form::open(['method' => 'PUT', 'route' => ['backend.prestasi.restore', Session::get('message') ]]) !!} {!! Form::button('Klik untuk UNDO', ['type' => 'submit', 'class' => 'btn waves-effect waves-light btn-outline-warning'] )  !!} {!! Form::close() !!}'
              });
    break;
    }
</script>
@endif
@endsection