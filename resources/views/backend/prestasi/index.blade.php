@extends('layout.backend.utama')

@section('konten')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Prestasi</h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Prestasi</a></li>
                            <li class="breadcrumb-item active">Semua Prestasi</li>
                        </ol>
                        <a href="{{ route('prestasi.create') }}" type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="icon-plus"></i> Tambah Data</a>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Data</h4>
                            <h6 class="card-subtitle">Daftar semua data Prestasi.</code></h6>
                            <!-- Nav tabs -->
                            <div class="vtabs customvtab">
                                <ul class="nav nav-tabs tabs-vertical" role="tablist">
                                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#utama" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Utama</span> </a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#tempatsampah" role="tab"><span class="hidden-sm-up"><i class="icon-trash"></i></span> <span class="hidden-xs-down">Tempat Sampah</span></a> </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content" style="width: 100%;">
                                    <div class="tab-pane active" id="utama" role="tabpanel">
                                        <div class="table-responsive">
                                            <table id="tableUtamaPrestasi" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Judul Prestasi</th>
                                                        <th>Tingkat</th>
                                                        <th>Dimenangkan</th>
                                                        <th>Isi</th>
                                                        <th>Tanggal</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($prestasi as $post)
                                                        <tr>
                                                            <td>{{ $post->title }}</td>
                                                            <td>{{ $post->tingkat }}</td>
                                                            <td>{{ $post->oleh }}</td>
                                                            <td>{{ $post->body }}</td>
                                                            <td>
                                                                <abbr title="{{ $post->dateFormatted(true) }}"> {{ $post->dateFormatted() }}
                                                                </abbr>
                                                                &nbsp;&nbsp;
                                                                {!! $post->publicationLabel() !!}
                                                            </td>
                                                            <td>
                                                                <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#confirmModal" data-whatever="@mdo"><i class="icon-trash"></i> Hapus</button>
                                                                <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel1">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title text-danger" id="confirmModalLabel1">KONFIRMASI</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <h4 class="text-danger">Apakah anda yakin akan menghapus Prestasi tersebut?</h4>
                                                                                <br>
                                                                                {!! Form::open(['method' => 'DELETE', 'route' => ['prestasi.destroy', $post->id]]) !!}
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit" class="btn btn-danger">Ya</button>
                                                                            </div>
                                                                                {!! Form::close() !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <a href="{{ route('prestasi.edit', $post->id) }}" type="button" class="btn btn-outline-info"><i class="icon-note"></i> Edit</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tempatsampah" role="tabpanel">
                                        <div class="table-responsive">
                                            <table id="tableTrashPostings" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Judul Prestasi</th>
                                                        <th>Dimenangkan</th>
                                                        <th>Isi</th>
                                                        <th>Tanggal</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($prestasiTrash as $posts)
                                                        <tr>
                                                            <td>{{ $posts->title }}</td>
                                                            <td>{{ $posts->oleh }}</td>
                                                            <td>{{ $posts->body }}</td>
                                                            <td>
                                                                <abbr title="{{ $posts->dateFormatted(true) }}"> {{ $posts->dateFormatted() }}
                                                                </abbr>
                                                            </td>
                                                            <td>
                                                        {!! Form::open(['method' => 'PUT', 'route' => ['backend.prestasi.restore', $posts->id]]) !!}

                                                                <button type="submit" class="btn btn-outline-primary"><i class="icon-reload"></i> Kembalikan</button>

                                                        {!! Form::close() !!}
                                                        &nbsp;
                                                        {!! Form::open(['method' => 'DELETE', 'route' => ['backend.prestasi.force-destroy', $posts->id]]) !!}

                                                                <button type="submit" class="btn btn-outline-danger"><i class="icon-close" onclick="return confirm('Anda yakin akan menghapus permanen?')"></i> Hapus Permanen</button>

                                                        {!! Form::close() !!}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
    </div>
@endsection

@include('backend.partials.message')
