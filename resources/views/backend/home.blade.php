@extends('layout.backend.utama')

@section('konten')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Dashboard</h4>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card border-success">
                        <div class="card-header bg-success">
                            <h4 class="m-b-0 text-white">Notifikasi</h4></div>
                        <div class="card-body">
                            <h3 class="card-title">Selamat datang {{ Auth::user()->name }}</h3>
                            <p class="card-text">Hari ini tanggal {{  now()->toDateTimeString() }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- Campaign -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="card ">
                        <img class="card-img" height="425" src="{{ asset('backend/assets/images/big/img1.jpg') }}" alt="Card image">
                        <div class="card-img-overlay card-inverse social-profile-first bg-over">
                            <img src="{{ asset('backend/assets/images/users/user.png') }}" class="img-circle" width="100">
                            <br>
                            <h2 class="card-title text-white">{{ Auth::user()->name }}</h2>
                        </div>
                        <div class="card-body text-center">
                            <div class="row">
                                <div class="col">
                                    <h3 class="m-b-0"></h3>
                                    <h5 class="font-light">{{ Auth::user()->email }}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <!-- column -->
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">USER</h5>
                                    <div class="d-flex m-t-30 m-b-20 no-block align-items-center">
                                        <span class="display-5 text-info"><i class="icon-people"></i></span>
                                        <a href="javscript:void(0)" class="link display-5 ml-auto">{{ $userCount }} Data</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- column -->
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">BLOG</h5>
                                    <div class="d-flex m-t-30 m-b-20 no-block align-items-center">
                                        <span class="display-5 text-purple"><i class="icon-pencil"></i></span>
                                        <a href="javscript:void(0)" class="link display-5 ml-auto">{{ $postingsCount }} Data</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- column -->
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">PRESTASI</h5>
                                    <div class="d-flex m-t-30 m-b-20 no-block align-items-center">
                                        <span class="display-5 text-primary"><i class="icon-star"></i></span>
                                        <a href="javscript:void(0)" class="link display-5 ml-auto">{{ $prestasiCount }} Data</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- column -->
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">KATEGORI</h5>
                                    <div class="d-flex m-t-30 m-b-20 no-block align-items-center">
                                        <span class="display-5 text-success"><i class="icon-pin"></i></span>
                                        <a href="javscript:void(0)" class="link display-5 ml-auto">{{ $kategoriCount }} Data</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- column -->
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Campaign -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
    </div>
@endsection
