@extends('layout.backend.utama')

@section('konten')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">Kategori</h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Kategori</a></li>
                            <li class="breadcrumb-item active">Daftar Kategori</li>
                        </ol>
                        <a href="{{ route('categories.create') }}" type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="icon-plus"></i> Tambah Data</a>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Data</h4>
                            <h6 class="card-subtitle">Daftar semua data Kategori.</code></h6>
                            <div class="table-responsive">
                                <table id="tableKategori" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Judul</th>
                                            <th>Slug</th>
                                            <th>Tanggal</th>
                                            <th>Jumlah Blog</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($categories as $cat)
                                            <tr>
                                                <td>{{ $cat->title }}</td>
                                                <td>{{ $cat->slug }}</td>
                                                <td>{{ $cat->created_at }}</td>
                                                <td><h4><span class="badge badge-success">{{ $cat->posts->count() }} Data</span></h4></td>
                                                <td>
                                    
                                            @if($cat->id != 1)

                                                    <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#confirmModal" data-whatever="@mdo"><i class="icon-trash"></i> Hapus</button>
                                                    <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel1">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title text-danger" id="confirmModalLabel1">KONFIRMASI</h4>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <h4 class="text-danger">Apakah anda yakin akan menghapus Kategori tersebut?</h4>
                                                                    <br>
                                                                    {!! Form::open(['method' => 'DELETE', 'route' => ['categories.destroy', $cat->id]]) !!}
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-danger">Ya</button>
                                                                </div>
                                                                    {!! Form::close() !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="{{ route('categories.edit', $cat->id) }}" type="button" class="btn btn-outline-info"><i class="icon-note"></i> Edit</a>

                                            @else

                                                    <h4><span class="badge badge-danger">Default</span></h4>

                                            @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
    </div>
@endsection

@include('backend.partials.message')
