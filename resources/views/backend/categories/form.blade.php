<form class="form-material m-t-40">
    <div class="form-group {{ $errors->has('title') ? 'has-danger' : '' }}">
        {!! Form::label('title', 'Judul') !!}
        {!! Form::text('title', null, ['class' => 'form-control form-control-line']) !!}

        @if($errors->has('title'))
            <small class="form-control-feedback">{{ $errors->first('title') }}</small>
        @endif
    </div>

    <div class="form-group {{ $errors->has('slug') ? 'has-danger' : '' }}">
        {!! Form::label('slug', 'Slug') !!}
        {!! Form::text('slug', null, ['class' => 'form-control form-control-line']) !!}

        @if($errors->has('slug'))
            <small class="form-control-feedback">{{ $errors->first('slug') }}</small>
        @endif
    </div>

    {!! Form::button('<i class="icon-check"></i> Simpan', ['type' => 'submit', 'class' => 'btn waves-effect waves-light btn-outline-success'] )  !!}
</form>